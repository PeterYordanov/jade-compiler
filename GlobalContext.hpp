#ifndef GLOBAL_CONTEXT_HPP
#define GLOBAL_CONTEXT_HPP

#include "llvm/IR/LLVMContext.h"

static llvm::LLVMContext GlobalContext;

#endif //GLOBAL_CONTEXT_HPP