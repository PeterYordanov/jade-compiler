#ifndef NODE_INTEGER_HPP
#define NODE_INTEGER_HPP

#include "NodeExpression.hpp"
#include "GlobalContext.hpp"
#include "CodeGenerationContext.hpp"
#include "Common.hpp"

class NodeInteger : public NodeExpression
{
public:
    long long value;
    NodeInteger(long long value) : 
        value(value) 
    {
    }

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_INTEGER_HPP