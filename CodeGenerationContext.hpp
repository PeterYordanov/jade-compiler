#ifndef CODE_GENERATION_CONTEXT_HPP
#define CODE_GENERATION_CONTEXT_HPP

#include <stack>
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h" 
#include "llvm/IR/Type.h" 
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/Support/raw_ostream.h"
#include "GlobalContext.hpp"
#include "CodeGenerationBlock.hpp"
#include "NodeIdentifier.hpp"
#include "NodeBlock.hpp"
#include "Common.hpp"

class CodeGenerationContext
{
private:
    std::stack<CodeGenerationBlock*> blocks;
    llvm::Function *mainFunction;

public:
    llvm::Module *module;
    CodeGenerationContext() { module = new llvm::Module("main", GlobalContext); }
    
    void generateCode(NodeBlock& root);

    llvm::GenericValue runCode();

    std::map<std::string, llvm::Value*>& locals() { return blocks.top()->locals; }
    llvm::BasicBlock *currentBlock() { return blocks.top()->block; }
    void pushBlock(llvm::BasicBlock *block);
    void popBlock();
};

#endif //CODE_GENERATION_CONTEXT_HPP