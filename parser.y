%{
    #include "Node.hpp"
    #include "NodeBlock.hpp"
    #include "NodeExpression.hpp"
    #include "NodeStatement.hpp"
    #include "NodeIdentifier.hpp"
    #include "NodeVariableDeclaration.hpp"
    NodeBlock* programBlock; /* the top level root node of our final AST */

    extern int yylex();
    void yyerror(const char *s) { printf("ERROR: %sn", s); }
%}

%union {
    Node* node;
    NodeBlock* block;
    NodeExpression* expr;
    NodeStatement* stmt;
    NodeIdentifier* ident;
    NodeVariableDeclaration* var_decl;
    std::vector<NodeVariableDeclaration*>* varvec;
    std::vector<NodeExpression*>* exprvec;
    std::string* string;
    int token;
}

%token <string> TIDENTIFIER TINTEGER TDOUBLE
%token <token> TCEQ TCNE TCLT TCLE TCGT TCGE TEQUAL
%token <token> TLPAREN TRPAREN TLBRACE TRBRACE TCOMMA TDOT
%token <token> TPLUS TMINUS TMUL TDIV

/* Define the type of node our nonterminal symbols represent.
   The types refer to the %union declaration above. Ex: when
   we call an ident (defined by union type ident) we are really
   calling an (NIdentifier*). It makes the compiler happy.
 */
%type <ident> ident
%type <expr> numeric expr 
%type <varvec> func_decl_args
%type <exprvec> call_args
%type <block> program stmts block
%type <stmt> stmt var_decl func_decl
%type <token> comparison

/* Operator precedence for mathematical operators */
%left TPLUS TMINUS
%left TMUL TDIV

%start program

%%

program : stmts { programBlock = $1; }
        ;
        
stmts : stmt { $$ = new NodeBlock(); $$->statements.push_back($<stmt>1); }
      | stmts stmt { $1->statements.push_back($<stmt>2); }
      ;

stmt : var_decl | func_decl
     | expr { $$ = new NodeExpressionStatement(*$1); }
     ;

block : TLBRACE stmts TRBRACE { $$ = $2; }
      | TLBRACE TRBRACE { $$ = new NodeBlock(); }
      ;

var_decl : ident ident { $$ = new NodeVariableDeclaration(*$1, *$2); }
         | ident ident TEQUAL expr { $$ = new NodeVariableDeclaration(*$1, *$2, $4); }
         ;
        
func_decl : ident ident TLPAREN func_decl_args TRPAREN block 
            { $$ = new NodeFunctionDeclaration(*$1, *$2, *$4, *$6); delete $4; }
          ;
    
func_decl_args : /*blank*/  { $$ = new std::vector<NodeVariableDeclaration*>(); }
          | var_decl { $$ = new std::vector<NodeVariableDeclaration*>(); $$->push_back($<var_decl>1); }
          | func_decl_args TCOMMA var_decl { $1->push_back($<var_decl>3); }
          ;

ident : TIDENTIFIER { $$ = new NodeIdentifier(*$1); delete $1; }
      ;

numeric : TINTEGER { $$ = new NodeInteger(atol($1->c_str())); delete $1; }
        | TDOUBLE { $$ = new NodeDouble(atof($1->c_str())); delete $1; }
        ;
    
expr : ident TEQUAL expr { $$ = new NodeAssignment(*$<ident>1, *$3); }
     | ident TLPAREN call_args TRPAREN { $$ = new NodeMethodCall(*$1, *$3); delete $3; }
     | ident { $<ident>$ = $1; }
     | numeric
     | expr comparison expr { $$ = new NodeBinaryOperator(*$1, $2, *$3); }
     | TLPAREN expr TRPAREN { $$ = $2; }
     ;
    
call_args : /*blank*/  { $$ = new std::vector<NodeExpression*>(); }
          | expr { $$ = new std::vector<NodeExpression*>(); $$->push_back($1); }
          | call_args TCOMMA expr  { $1->push_back($3); }
          ;

comparison : TCEQ | TCNE | TCLT | TCLE | TCGT | TCGE 
           | TPLUS | TMINUS | TMUL | TDIV
           ;

%%