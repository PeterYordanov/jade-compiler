#include "NodeDouble.hpp"

    llvm::Value* NodeDouble::codeGen(CodeGenerationContext& context)
    {
        std::cout << "Creating double: " << value << std::endl;
        return ConstantFP::get(Type::getDoubleTy(GlobalContext), value);
    }