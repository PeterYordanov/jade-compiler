#include "NodeVariableDeclaration.hpp"

llvm::Value* NodeVariableDeclaration::codeGen(CodeGenerationContext& context)
{
    std::cout << "Creating variable declaration " << type.name << " " << id.name << std::endl;
    llvm::AllocaInst *alloc = new llvm::AllocaInst(const_cast<llvm::Type*>(typeOf(type)), id.name.c_str(), context.currentBlock());
    context.locals()[id.name] = alloc;
    if (assignmentExpr != NULL) {
        NodeAssignment assn(id, *assignmentExpr);
        assn.codeGen(context);
    }
    return alloc;
}