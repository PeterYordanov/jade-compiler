#include "NodeIdentifier.hpp"

llvm::Value* NodeIdentifier::codeGen(CodeGenerationContext& context)
{
    std::cout << "Creating identifier reference: " << name << std::endl;
    if (context.locals().find(name) == context.locals().end()) {
        std::cerr << "undeclared variable " << name << std::endl;
        return NULL;
    }
    return new llvm::LoadInst(context.locals()[name], "", false, context.currentBlock());
}