#include "NodeExpressionStatement.hpp"

llvm::Value* NodeExpressionStatement::codeGen(CodeGenerationContext& context)
    {
        std::cout << "Generating code for " << typeid(expression).name() << std::endl;
        return expression.codeGen(context);
    }