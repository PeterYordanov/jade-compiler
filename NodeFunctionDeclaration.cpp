#include "NodeFunctionDeclaration.hpp"
#include "GlobalContext.hpp"

llvm::Value* NodeFunctionDeclaration::codeGen(CodeGenerationContext& context)
    {
        std::vector<const llvm::Type*> argTypes;
        std::vector<NodeVariableDeclaration*>::const_iterator it;
        for (it = arguments.begin(); it != arguments.end(); it++) {
            argTypes.push_back(typeOf((**it).type));
        }
        llvm::FunctionType *ftype = FunctionType::get(typeOf(type), argTypes, false);
        llvm::Function *function = Function::Create(ftype, GlobalValue::InternalLinkage, id.name.c_str(), context.module);
        llvm::BasicBlock *bblock = BasicBlock::Create(GlobalContext, "entry", function, 0);

        context.pushBlock(bblock);

        for (it = arguments.begin(); it != arguments.end(); it++) {
            (**it).codeGen(context);
        }
    
        block.codeGen(context);
        llvm::ReturnInst::Create(GlobalContext, bblock);

        context.popBlock();
        std::cout << "Creating function: " << id.name << std::endl;
        return function;
    }