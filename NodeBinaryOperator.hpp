#ifndef NODE_BINARY_OPERATOR_HPP
#define NODE_BINARY_OPERATOR_HPP

#include "NodeExpression.hpp"
#include "Common.hpp"

class NodeBinaryOperator : public NodeExpression
{
public:
    int op;
    NodeExpression& lhs;
    NodeExpression& rhs;
    
    NodeBinaryOperator(NodeExpression& lhs, int op, NodeExpression& rhs) :
        lhs(lhs), 
        rhs(rhs), 
        op(op) 
    {
    }

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_BINARY_OPERATOR_HPP