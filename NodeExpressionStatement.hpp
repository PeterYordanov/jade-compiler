#ifndef NODE_EXPRESSION_STATEMENT_HPP
#define NODE_EXPRESSION_STATEMENT_HPP

#include "NodeStatement.hpp"
#include "Common.hpp"

class NodeExpressionStatement : public NodeStatement
{
public:
    NodeExpression& expression;
    NodeExpressionStatement(NodeExpression& expression) : 
        expression(expression) 
    {
    }

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_EXPRESSION_STATEMENT_HPP