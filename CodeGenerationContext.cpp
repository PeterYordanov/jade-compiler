#include "CodeGenerationContext.hpp"

void CodeGenerationContext::generateCode(NodeBlock& root)
    {
        std::cout << "Generating code...n";
    
        std::vector<const llvm::Type*> argTypes;
        llvm::FunctionType *ftype = llvm::FunctionType::get(llvm::Type::getVoidTy(GlobalContext), argTypes, false);
        mainFunction = llvm::Function::Create(ftype, llvm::GlobalValue::InternalLinkage, "main", module);
        llvm::BasicBlock *bblock = llvm::BasicBlock::Create(GlobalContext, "entry", mainFunction, 0);
        
        pushBlock(bblock);
        root.codeGen(*this);
        llvm::ReturnInst::Create(GlobalContext, bblock);
        popBlock();
        
        std::cout << "Code is generated.n";
        llvm::PassManager pm;
        pm.add(llvm::createPrintModulePass(llvm::outs()));
        pm.run(*module);
    }

    llvm::GenericValue CodeGenerationContext::runCode()
    {
        std::cout << "Running code...n";
        std::unique_ptr<llvm::Module> unique_module(module);

        llvm::EngineBuilder engineBuilder(std::move(unique_module));
        std::string errStr;
        engineBuilder.setErrorStr( &errStr );
        engineBuilder.setEngineKind( llvm::EngineKind::JIT );
        //llvm::ExistingModuleProvider *mp = new llvm::ExistingModuleProvider(module);
        llvm::ExecutionEngine *ee = engineBuilder.create();//llvm::ExecutionEngine::create();
        std::vector<llvm::GenericValue> noargs;
        llvm::GenericValue v = ee->runFunction(mainFunction, noargs);
        std::cout << "Code was run.n";
        return v;
    }


void CodeGenerationContext::pushBlock(llvm::BasicBlock *block) 
{ 
    blocks.push(new CodeGenerationBlock()); 
    blocks.top()->block = block; 
}

void CodeGenerationContext::popBlock() 
{ 
    CodeGenerationBlock *top = blocks.top(); 
    blocks.pop(); 
    delete top;
}