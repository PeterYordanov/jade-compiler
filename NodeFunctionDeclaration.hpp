#ifndef NODE_FUNCTION_DECLARATION_HPP
#define NODE_FUNCTION_DECLARATION_HPP

#include "NodeStatement.hpp"
#include "NodeVariableDeclaration.hpp"
#include "NodeBlock.hpp"
#include "CodeGenerationContext.hpp"
#include "Common.hpp"

class NodeFunctionDeclaration : public NodeStatement
{
public:
    const NodeIdentifier& type;
    const NodeIdentifier& id;
    std::vector<NodeVariableDeclaration*> arguments;
    NodeBlock& block;

    NodeFunctionDeclaration(const NodeIdentifier& type, const NodeIdentifier& id, 
            const std::vector<NodeVariableDeclaration*>& arguments, NodeBlock& block) :
        type(type), 
        id(id), 
        arguments(arguments), 
        block(block) 
    {
    }

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_FUNCTION_DECLARATION_HPP