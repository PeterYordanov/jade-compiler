#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>
#include <vector>
#include "CodeGenerationContext.hpp"
#include "Common.hpp"

class Node
{
public:
    virtual ~Node() {}
    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_HPP