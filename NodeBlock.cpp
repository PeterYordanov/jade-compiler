#include "NodeBlock.hpp"

llvm::Value* NodeBlock::codeGen(CodeGenerationContext& context)
{
    std::vector<NodeStatement*>::const_iterator it;
    llvm::Value *last = NULL;
    for (it = statements.begin(); it != statements.end(); it++) {
        std::cout << "Generating code for " << typeid(**it).name() << std::endl;
        last = (**it).codeGen(context);
    }
    std::cout << "Creating block" << std::endl;
    return last;
}