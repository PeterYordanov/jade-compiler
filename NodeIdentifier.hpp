#ifndef NODE_IDENTIFIER_HPP
#define NODE_IDENTIFIER_HPP

#include "CodeGenerationContext.hpp"
#include "NodeExpression.hpp"
#include "Common.hpp"

class NodeIdentifier : public NodeExpression
{
public:
    std::string name;
    NodeIdentifier(const std::string& name) : 
        name(name) 
    {
    }
 
    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_IDENTIFIER_HPP