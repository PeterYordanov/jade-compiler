#ifndef COMMON_HPP
#define COMMON_HPP

#include <stack>
#include "NodeIdentifier.hpp"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h" 
#include "llvm/IR/Type.h" 
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/Support/raw_ostream.h"
#include "GlobalContext.hpp"

inline const llvm::Type* typeOf(const NodeIdentifier& type) 
{
    if (type.name.compare("int") == 0) {
        return llvm::Type::getInt64Ty(GlobalContext);
    }
    else if (type.name.compare("double") == 0) {
        return llvm::Type::getDoubleTy(GlobalContext);
    }
    return llvm::Type::getVoidTy(GlobalContext);
}

#endif //COMMON_HPP