#include "NodeAssignment.hpp"

llvm::Value* NodeAssignment::codeGen(CodeGenerationContext& context)
    {
        std::cout << "Creating assignment for " << lhs.name << std::endl;
        if (context.locals().find(lhs.name) == context.locals().end()) {
            std::cerr << "undeclared variable " << lhs.name << std::endl;
            return NULL;
        }
        return new llvm::StoreInst(rhs.codeGen(context), context.locals()[lhs.name], false, context.currentBlock());
    }