#ifndef NODE_EXPRESSION_HPP
#define NODE_EXPRESSION_HPP

#include "Node.hpp"

#include <stack>
#include "NodeIdentifier.hpp"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h" 
#include "llvm/IR/Type.h" 
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/Support/raw_ostream.h"
#include "Common.hpp"

class NodeExpression : public Node
{

};

using namespace std;
using namespace llvm;

#endif //NODE_EXPRESSION_HPP