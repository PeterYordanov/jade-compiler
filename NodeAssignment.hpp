#ifndef NODE_ASSIGNMENT_HPP
#define NODE_ASSIGNMENT_HPP

#include "NodeExpression.hpp"
#include "Common.hpp"

class NodeAssignment : public NodeExpression
{
public:
    NodeIdentifier& lhs;
    NodeExpression& rhs;

    NodeAssignment(NodeIdentifier& lhs, NodeExpression& rhs) : 
        lhs(lhs), 
        rhs(rhs) 
    {
    }
 
    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_ASSIGNMENT_HPP