#include <iostream>
#include "CodeGenerationContext.hpp"
#include "CodeGenerationBlock.hpp"
#include "Node.hpp"

extern int yyparse();
extern NodeBlock* programBlock;

int main(int argc, char* argv[])
{
    yyparse();
    std::cout << programBlock << std::endl;

    CodeGenerationContext context;
    context.generateCode(*programBlock);
    context.runCode();
    
    return 0;
}