#include "NodeInteger.hpp"

llvm::Value* NodeInteger::codeGen(CodeGenerationContext& context)
    {
        std::cout << "Creating integer: " << value << std::endl;
        return ConstantInt::get(Type::getInt64Ty(GlobalContext), value, true);
    }