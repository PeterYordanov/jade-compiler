#ifndef NODE_DOUBLE_HPP
#define NODE_DOUBLE_HPP

#include "NodeExpression.hpp"
#include "NodeDouble.hpp"
#include "CodeGenerationContext.hpp"
#include "Common.hpp"

class NodeDouble : public NodeExpression
{
public:
    double value;
    NodeDouble(double value) : 
        value(value) 
    {
    }

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_DOUBLE_HPP