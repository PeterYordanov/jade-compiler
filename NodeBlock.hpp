#ifndef NODE_BLOCK_HPP
#define NODE_BLOCK_HPP

#include "CodeGenerationContext.hpp"
#include "NodeExpression.hpp"
#include "NodeStatement.hpp"
#include "Common.hpp"

class NodeBlock : public NodeExpression
{
public:
    std::vector<NodeStatement*> statements;
    
    NodeBlock() = default;

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_BLOCK_HPP