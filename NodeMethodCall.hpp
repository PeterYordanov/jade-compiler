#ifndef NODE_METHOD_CALL_HPP
#define NODE_METHOD_CALL_HPP

#include "NodeIdentifier.hpp"
#include "NodeExpression.hpp"
#include "NodeGenerationContext.hpp"
#include "Common.hpp"

class NodeMethodCall : public NodeExpression {
public:
    const NodeIdentifier& id;
    std::vector<NodeExpression*>& arguments;
    NodeMethodCall(const NodeIdentifier& id, std::vector<NodeExpression*>& arguments) :
        id(id), 
        arguments(arguments) 
    {
    }

    NodeMethodCall(const NodeIdentifier& id) : id(id) { }
    
    llvm::Value* codeGen(CodeGenerationContext& context)
    {
        llvm::Function *function = context.module->getFunction(id.name.c_str());
        if (function == NULL) {
            std::cerr << "no such function " << id.name << std::endl;
        }
        std::vector<llvm::Value*> args;
        std::vector<NodeExpression*>::const_iterator it;
        for (it = arguments.begin(); it != arguments.end(); it++) {
            args.push_back((**it).codeGen(context));
        }
        llvm::CallInst* call = llvm::CallInst::Create(function, args.begin(), args.end(), "", context.currentBlock());
        std::cout << "Creating method call: " << id.name << std::endl;
        return call;
    }
};

#endif //NODE_METHOD_CALL_HPP