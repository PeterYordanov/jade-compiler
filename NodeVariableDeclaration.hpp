#ifndef NODE_VARIABLE_DECLARATION_HPP
#define NODE_VARIABLE_DECLARATION_HPP

#include "CodeGenerationContext.hpp"
#include "NodeStatement.hpp"
#include "Common.hpp"

class NodeVariableDeclaration : public NodeStatement
{
public:
    const NodeIdentifier& type;
    NodeIdentifier& id;
    NodeExpression* assignmentExpr;
    NodeVariableDeclaration(const NodeIdentifier& type, NodeIdentifier& id) :
        type(type), 
        id(id) 
    {
    }

    NodeVariableDeclaration(const NodeIdentifier& type, NodeIdentifier& id, NodeExpression *assignmentExpr) :
        type(type), 
        id(id), 
        assignmentExpr(assignmentExpr) 
    {
    }

    llvm::Value* codeGen(CodeGenerationContext& context);
};

#endif //NODE_VARIABLE_DECLARATION_HPP